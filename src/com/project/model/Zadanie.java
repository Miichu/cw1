package com.project.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "zadanie")
public class Zadanie {
	@Id
	@GeneratedValue
	@Column(name = "zadanie_id", nullable = false)
	private Integer zadanieId;

	
	@Column(name = "nazwa", nullable = false)
	private String nazwa;

	@Column(name = "kolejnosc")
	private Integer Kolejnosc;

	@Column(name = "opis")
	
	private String opis;

	@Column(name = "dataczas_dodania", nullable = false, updatable = false)
	private LocalDateTime dataczas_dodania;
	
	
	@ManyToOne
	@JoinColumn(name = "projekt_id")
	private Projekt projekt;
	
	public Zadanie() {
	}

	public Integer getZadanieId() {
		return zadanieId;
	}

	public void setZadanieId(Integer zadanieId) {
		this.zadanieId = zadanieId;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Integer getKolejnosc() {
		return Kolejnosc;
	}

	public void setKolejnosc(Integer kolejnosc) {
		Kolejnosc = kolejnosc;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public LocalDateTime getDataczas_dodania() {
		return dataczas_dodania;
	}

	public void setDataczas_dodania(LocalDateTime dataczas_dodania) {
		this.dataczas_dodania = dataczas_dodania;
	}

	public Projekt getProjekt() {
		return projekt;
	}

	public void setProjekt(Projekt projekt) {
		this.projekt = projekt;
	}
	
	

}
